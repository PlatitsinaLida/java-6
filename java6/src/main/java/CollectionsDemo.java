
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;


public class CollectionsDemo {

    //Вход: список строк и символ. Выход: количество строк входного списка, у которых первый
// символ совпадает с заданным.
    public static int numberLinesFirstSymbol(List<String> list, char symbol){
        int num = 0;
         for (String str:list)
        {
            /* Филиппов А.В. 19.06.2020 Комментарий не удалять.
             Зачем тут toLowerCase()?
            */
            if(str.toLowerCase().charAt(0) == symbol)
            {
                num ++;
            }
        }
         return num;
    }

    //Вход: список объектов типа Human и еще один объект типа Human. Выход — копия входного списка, не содержащая
    // выделенного человека.При изменении элементов входного списка элементы выходного изменяться не должны.
    public static List<Human> copyListWithoutOnePerson(List<Human> list ,Human person){
        List<Human> copyList = new ArrayList<>();

        for (int i = 0; i < list.size(); i++) {
            if (!list.get(i).equals(person)) {
                copyList.add(new Human(list.get(i)));
            }

        }

        return copyList;
    }

    //Вход: список множеств целых чисел и еще одно множество. Выход: список всех множеств входного списка,
    // которые не пересекаются с заданным множеством.
    public static List<Set<Integer>> unionOfSets(List<Set<Integer>> set, Set<Integer> setInteger) {
        List<Set<Integer>> result = new ArrayList<Set<Integer>>();
        /* Филиппов А.В. 19.06.2020 Комментарий не удалять.
         Объявив эту переменную внутри цикл вы бы избавились от потенциальной ошибки: можно забыть
         установить эту переменную в конце цикла.
        */
        Iterator<Integer> iterator = setInteger.iterator();
        for (int i = 0; i < set.size(); i++) {
            while (iterator.hasNext()) {
                if (set.get(i).contains(iterator.next())) {
                    break;
                }
                if (!iterator.hasNext()) {
                    result.add(set.get(i));
                }
            }
            iterator = setInteger.iterator();
        }
        return result;
    }

    //

}
