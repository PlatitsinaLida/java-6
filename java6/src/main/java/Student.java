//Создайте класс Student, производный от Human, новое поле — название факультета, к нему геттер, сеттер и конструктор.
public class Student extends Human {
    private String faculty;

    public Student(String lastName, String firstName, String middleName, int age, String faculty) {
        super(lastName, firstName, middleName, age);
        this.faculty = faculty;
    }

    public Student(Human copy, String faculty) {
        super(copy);
        this.faculty = faculty;
    }

    public String getFaculty() {
        return faculty;
    }
    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }
}
