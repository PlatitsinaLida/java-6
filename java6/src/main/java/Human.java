import java.util.Objects;

//Создайте класс Human с полями: фамилия, имя, отчество,
// возраст и методами: конструкторы, геттеры/сеттеры, equals и hashCode.
public class Human {
    private String lastName;//фамилия
    private String firstName;//имя
    private String middleName;//отчество
    private int age;

    public Human(String lastName, String firstName, String middleName, int age) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.middleName = middleName;
        this.age = age;
    }

    public Human(Human copy) {
        this.lastName = copy.lastName;
        this.firstName = copy.firstName;
        this.middleName = copy.middleName;
        this.age = copy.age;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        if (lastName == null || "".equals(lastName)) {
            throw new IllegalArgumentException("Last name's Error");
        }
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        if (firstName == null || "".equals(firstName)) {
            throw new IllegalArgumentException("First name's Error");
        }
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        if (middleName == null || "".equals(middleName)) {
            throw new IllegalArgumentException("Middle name's Error");
        }
        this.middleName = middleName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if (age < 0) {
            throw new IllegalArgumentException("Age's Error");
        }
        this.age = age;
    }

    @Override
    public String toString() {
        return "Human{" +
                "lastName='" + lastName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", age=" + age +
                '}';
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Human)) return false;
        Human human = (Human) o;
        return age == human.age &&
                Objects.equals(lastName, human.lastName) &&
                Objects.equals(firstName, human.firstName) &&
                Objects.equals(middleName, human.middleName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(lastName, firstName, middleName, age);
    }
}
