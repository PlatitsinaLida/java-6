import java.util.*;

public class ListDemo {

   //Напишите метода класса ListDemo, который получает на вход список объектов типа Human и еще один объект типа Human.
   // Результат — список однофамильцев заданного человека среди людей из входного списка.
    public static List<Human> namesake(List<Human> list,Human human){
        List<Human> result = new ArrayList<>();
        for (int i=0; i<list.size();i++){
           if (list.get(i).getLastName().equals(human.getLastName())){
               result.add(new Human(list.get(i)));
           }
        }
        return result;

    }

    //Напишите метод класса ListDemo, который получает на вход список, состоящий из объектов типа Human и его
    // производных классов. Результат — множество людей из входного списка с максимальным возрастом.
    public static Set<Human> maxAge(List<Human> list){
        Set<Human> result = new HashSet<>();
        int maxAge = 0;
        for (int i = 0; i < list.size(); i++) {
            if (maxAge < list.get(i).getAge()) {
                result.clear();
                maxAge = list.get(i).getAge();
                result.add(list.get(i));
            }
            if (maxAge == list.get(i).getAge()) {
                result.add(list.get(i));
            }
        }
        return result;
    }

    //Задача 7
    //Имеется набор людей, каждому человеку задан уникальный целочисленный идентификатор. Напишите метод,
    // который получает на вход отображение (Map) целочисленных идентификаторов в объекты типа Human и
    // множество целых чисел.Результат — множество людей, идентификаторы которых содержатся во входном множестве.

    /* Филиппов А.В. 19.06.2020 Комментарий не удалять.
     Не работает! Нет теста. Вернее он есть, но проверяет только то, что функция запускается.
    */
    public static Set<Human> findHumans(Map<Integer, Human> map, Set<Integer> setInteger) {
        Set<Human> setHumans = new HashSet<Human>();
        int key = 0;
        /* Филиппов А.В. 19.06.2020 Комментарий не удалять.
         Явно итераторы уже не используются. Нажмите альт-ентер на while и выберите первый пунт улучшений от идеи.
        */
        for (Integer integer : setInteger) {
            key = integer;
            if (map.containsKey(key)) {
                setHumans.add(map.get(key));
            }
        }
        return setHumans;
    }

    //Для отображения из задачи 7(findHumans) постройте список идентификаторов людей, чей возраст не менее 18 лет.
    public static List<Integer> AgeMore18 (Map<Integer, Human> map){
        List<Integer> listInteger = new ArrayList<Integer>();
        Set<Integer> integerSet = map.keySet();
        int key;
        Iterator<Integer> iterator = integerSet.iterator();
        while(iterator.hasNext()){
            key = iterator.next();
            if(map.get(key).getAge()>=18){
                listInteger.add(key);
            }
        }
        return listInteger;
    }

    //Для отображения из задачи 7 постройте новое отображение, которое идентификатору сопоставляет возраст человека.
    public static Map<Integer, Integer> idForAge(Map<Integer, Human> mapId) {
        Map<Integer, Integer> result= new HashMap<>();
        for (Integer i : mapId.keySet()) {
            result.put(i, mapId.get(i).getAge());

        }

        return result;
    }
    private static List<Human> f(int age, Set<Human> humans) {
        List<Human> result = new ArrayList<>();
        for (Human h : humans) {
            if (age == h.getAge()) {
                result.add(h);
            }
        }
        return result;
    }
    //По множеству объектов типа Human постройте отображение, которое целому числу (возраст человека)
    // сопоставляет список всех людей данного возраста из входного множества.
    public static Map<Integer, List<Human>> listAge(Set<Human> humans) {
        Map<Integer, List<Human>> result = new HashMap<>();
        for (Human h : humans) {
            int age = h.getAge();
            if (!result.containsKey(age)) {
                result.put(age, f(age, humans));
            }
        }

        return result;
    }
}
