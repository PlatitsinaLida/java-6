import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestListDemo {
    @Test
    public void testGetListSameSurnameWithHenry() {
        Human goodHenry = new Human("Jhonson", "Henry", "Baker", 45);
        List<Human> list = new ArrayList<>();
        List<Human> res = new ArrayList<>();
        list.add(new Human("Ivanov", "Ivan", "Ivanovich", 66));
        list.add(new Human(goodHenry));
        res.add(new Human(goodHenry));
        list.add(new Human("Jhonson", "Timmy", "Cloud", 25));
        res.add(new Human("Jhonson", "Timmy", "Cloud", 25));
        list.add(new Human("Jhonson", "Boris", "Petrovich", 125));
        res.add(new Human("Jhonson", "Boris", "Petrovich", 125));
        list.add(new Human("Jhonsons", "Boriz", "Zovich", 87));
        list.add(new Human("Rainbower", "Zins", "Chovich", 77));
        list.add(new Human("Jhonson", "Reobert", "Grober", 7));
        res.add(new Human("Jhonson", "Reobert", "Grober", 7));
        assertEquals(ListDemo.namesake(list, goodHenry), res);
    }
    @Test
    public void testMaxAge() {
        List<Human> obj = new ArrayList<>();
        Set<Human> res = new HashSet<>();
        obj.add(new Student("Kllda", "Adsds", "Qqadsa", 42, "Ll"));
        obj.add(new Student("Sllda", "Adsds", "Qqadsa", 42, "Ll"));
        obj.add(new Student("Zllda", "Adsds", "Qqadsa", 42, "Ll"));
        obj.add(new Human("Kll", "Ads", "Dsa", 12));
        obj.add(new Human("Kll", "Ads", "Dsa", 42));
        obj.add(new Human("Sdll", "Ads", "Dsa", 42));
        obj.add(new Human("SdKll", "Ads", "Dsa", 12));
        obj.add(new Human("Kllda", "Adsds", "Qqadsa", 82));
        obj.add(new Human("Kllda", "Adsds", "Qqadsa", 82));
        obj.add(new Human("Qllda", "Xdsds", "Cqadsa", 82));
        obj.add(new Student("Kllda", "Adsds", "Qqadsa", 82, "Ll"));
        obj.add(new Human("SdKll", "Ads", "Dsa", 12));
        obj.add(new Student("Kllda", "Adsds", "Qqadsa", 42, "Ll"));
        obj.add(new Student("Kllda", "Adsds", "Qqadsa", 72, "Ll"));
        obj.add(new Human("SdKll", "Ads", "Dsa", 12));
        obj.add(new Human("SdKll", "Ads", "Dsa", 12));
        obj.add(new Student("QqKllda", "Adsds", "Qqadsa", 82, "Ll"));
        obj.add(new Student("QqKllda", "Adsds", "Qqadsa", 82, "Ll"));
        obj.add(new Student("QqKllda", "Adsds", "Qqadsa", 82, "LlD"));
        obj.add(new Student("Kllda", "Adsds", "Qqadsa", 82, "Ll"));
        obj.add(new Student("Kllda", "Adsds", "Qqadsa", 82, "Ll"));
        obj.add(new Human("Adsllda", "aAdsds", "Qqadsa", 82));
        obj.add(new Human("AdsKllda", "Adsds", "Qqadsa", 82));
        obj.add(new Human("dsdKllda", "sAdsds", "Qqadsa", 52));
        obj.add(new Student("QqKllda", "Adsds", "Qqadsa", 82, "Ll"));
        res.add(new Human("Kllda", "Adsds", "Qqadsa", 82));
        res.add(new Human("Qllda", "Xdsds", "Cqadsa", 82));
        res.add(new Student("Kllda", "Adsds", "Qqadsa", 82, "Ll"));
        res.add(new Student("QqKllda", "Adsds", "Qqadsa", 82, "Ll"));
        res.add(new Student("QqKllda", "Adsds", "Qqadsa", 82, "LlD"));
        res.add(new Human("Adsllda", "aAdsds", "Qqadsa", 82));
        res.add(new Human("AdsKllda", "Adsds", "Qqadsa", 82));
        assertEquals(ListDemo.maxAge(obj), res);
    }
    @Test
    public void testIdentifeHumans() {
        Map<Integer, Human> mapId = new HashMap<>();
        Set<Integer> setOfId = new HashSet<>();
        Set<Human> res = new HashSet<>();
        mapId.put(1, new Human("Fdd", "Adds", "Acss", 14));
        mapId.put(2, new Human("Edd", "Redsds", "Vbss", 30));
        mapId.put(22, new Human("Zss", "Forsds", "Qrss", 22));
        mapId.put(220, new Human("AsdfZss", "Forsds", "Qrss", 22));
        mapId.put(22475, new Human("QrZss", "GfsdForsds", "SDdQrss", 22));
        mapId.put(25, new Human("Oppsd", "Klos", "Lks", 53));
        mapId.put(117, new Human("Black", "Monds", "Qwre", 1));
        setOfId.add(1);
        setOfId.add(2);
        setOfId.add(22);
        setOfId.add(2774);
        setOfId.add(36);
        setOfId.add(3);
        setOfId.add(25);
        setOfId.add(87);
        setOfId.add(110);
        setOfId.add(117);
        setOfId.add(10);
        setOfId.add(19);
        res.add(new Human("Fdd", "Adds", "Acss", 14));
        res.add(new Human("Edd", "Redsds", "Vbss", 30));
        res.add(new Human("Zss", "Forsds", "Qrss", 22));
        res.add(new Human("Oppsd", "Klos", "Lks", 53));
        res.add(new Human("Black", "Monds", "Qwre", 1));

    assertTrue(ListDemo.findHumans(mapId, setOfId).equals(res));
    }

    @Test
    public void testIdForAge() {
        Map<Integer, Human> mapId = new HashMap<>();
        Map<Integer, Integer> res = new HashMap<>();
        mapId.put(1, new Human("Fdd", "Adds", "Acss", 14));
        mapId.put(2, new Human("Edd", "Redsds", "Vbss", 30));
        mapId.put(22, new Human("Zss", "Forsds", "Qrss", 22));
        mapId.put(220, new Human("AsdfZss", "Forsds", "Qrss", 22));
        res.put(1, 14);
        res.put(2, 30);
        res.put(22, 22);
        res.put(220, 22);
        assertEquals(res, ListDemo.idForAge(mapId));

    }
    @Test
    public void testGetIdAge() {
        Map<Integer, Human> mapId = new TreeMap<>();
        Set<Integer> res = new TreeSet<>();
        mapId.put(1, new Human("Fdd", "Adds", "Acss", 14));
        mapId.put(2, new Human("Edd", "Redsds", "Vbss", 30));
        mapId.put(22, new Human("Zss", "Forsds", "Qrss", 22));
        mapId.put(220, new Human("AsdfZss", "Forsds", "Qrss", 22));
        mapId.put(22475, new Human("QrZss", "GfsdForsds", "SDdQrss", 17));
        mapId.put(25, new Human("Oppsd", "Klos", "Lks", 53));
        mapId.put(117, new Human("Black", "Monds", "Qwre", 1));
        mapId.put(2117, new Human("Redflack", "Monds", "Qwre", 18));
        res.add(2);
        res.add(22);
        res.add(25);
        res.add(220);
        res.add(2117);
        List<Integer> s=new ArrayList<>();
        s.addAll(res);
        List<Integer> r=ListDemo.AgeMore18(mapId);
        assertEquals(s,r);
    }
    @Test
    public void testGetAgeMap() {
        Set<Human> obj = new HashSet<>();
        Map<Integer, List<Human>> res = new HashMap<>();
        List<Human> list=new ArrayList<>();
        List<Human> list1=new ArrayList<>();
        List<Human> list2=new ArrayList<>();
        List<Human> list3=new ArrayList<>();
        List<Human> list4=new ArrayList<>();
        list.add(new Human("Abcd", "Bbc", "Cdd", 18));
        res.put(18,list);
        list1.add(new Human("Ad", "Ac", "Cd", 78));
        list1.add(new Human("Abcd", "Aac", "Cdd", 78));
        res.put(78,list1);
        list2.add(new Human("Abcd", "Abc", "Vdd", 178));
        res.put(178,list2);
        list3.add(new Human("Zebcd", "Abc", "Cdd", 14));
        res.put(14,list3);
        list4.add(new Human("Abcd", "Dbc", "Cdd", 32));
        res.put(32,list4);
        obj.add(new Human("Abcd", "Bbc", "Cdd", 18));
        obj.add(new Human("Ad", "Ac", "Cd", 78));
        obj.add(new Human("Abcd", "Aac", "Cdd", 78));
        obj.add(new Human("Abcd", "Abc", "Vdd", 178));
        obj.add(new Human("Zebcd", "Abc", "Cdd", 14));
        obj.add(new Human("Abcd", "Dbc", "Cdd", 32));

        assertEquals(res, ListDemo.listAge(obj));

    }




}
