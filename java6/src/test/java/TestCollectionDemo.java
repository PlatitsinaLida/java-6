import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class TestCollectionDemo {
    @Test
    public void testNumberLinesFirstSymbol(){
        List<String> list=new ArrayList<>();
        list.add("Yasdf");
        list.add("ASDFGHJ");
        list.add("AcHKL");
        list.add("axghj");
        assertEquals(3, CollectionsDemo.numberLinesFirstSymbol(list, 'a'));
    }
    @Test
    public void testCopyHenryFromList() {
        Human goodHenry = new Human("Jhonson", "Henry", "Baker", 45);
        List<Human> list = new ArrayList<>();
        List<Human> res = new ArrayList<>();
        list.add(new Human("Jhonson", "Henry", "Baker", 45));
        list.add(new Human("Ivanov", "Ivan", "Ivanovich", 66));
        res.add(new Human("Ivanov", "Ivan", "Ivanovich", 66));
        list.add(new Human(goodHenry));
        list.add(new Human("Jhonson", "Timmy", "Cloud", 25));
        res.add(new Human("Jhonson", "Timmy", "Cloud", 25));
        list.add(new Human("Jhonson", "Boris", "Petrovich", 125));
        res.add(new Human("Jhonson", "Boris", "Petrovich", 125));
        list.add(new Human(goodHenry));
        list.add(new Human("Jhonsons", "Boriz", "Zovich", 87));
        res.add(new Human("Jhonsons", "Boriz", "Zovich", 87));
        list.add(new Human("Rainbower", "Zins", "Chovich", 77));
        list.add(new Human(goodHenry));
        list.add(new Human(goodHenry));
        res.add(new Human("Rainbower", "Zins", "Chovich", 77));
        list.add(new Human("Jhonson", "Reobert", "Grober", 7));
        list.add(new Human(goodHenry));
        res.add(new Human("Jhonson", "Reobert", "Grober", 7));
        assertEquals(CollectionsDemo.copyListWithoutOnePerson(list, goodHenry), res);
        assertEquals(list.get(1), res.get(0));
        list.set(1, new Human(goodHenry));
        assertNotEquals(list.get(1), res.get(0));
    }
    @Test
    public void testUnionIntersectSets() {
        Set<Integer> set = new HashSet<>();
        set.add(1);
        set.add(2);
        set.add(3);
        set.add(4);
        set.add(5);
        Set<Integer> set1 = new HashSet<>();//-
        Set<Integer> set2 = new HashSet<>();//+
        Set<Integer> set3 = new HashSet<>();//+
        Set<Integer> set4 = new HashSet<>();//-
        Set<Integer> set5 = new HashSet<>();//-
        Set<Integer> set6 = new HashSet<>();//+
        Set<Integer> set7 = new HashSet<>();//+
        set1.add(1);
        set1.add(9);
        set1.add(0);
        set1.add(91);
        set2.add(20);
        set2.add(12);
        set2.add(974);
        set2.add(87);
        set2.add(78);
        set2.add(45);
        set3.add(0);
        set4.add(1);
        set4.add(2);
        set4.add(3);
        set4.add(4);
        set4.add(5);
        set5.add(8);
        set5.add(10);
        set5.add(85);
        set5.add(18);
        set5.add(4);
        set5.add(102);
        set7.add(45);
        set7.add(123);
        set7.add(1234);
        set7.add(12345);
        List<Set<Integer>> list = new ArrayList<>();
        list.add(set1);
        list.add(set2);
        list.add(set3);
        list.add(set4);
        list.add(set5);
        list.add(set6);
        list.add(set7);
        list.add(set4);
        list.add(set6);
        list.add(set7);
        list.add(set2);
        list.add(set4);
        list.add(set6);
        list.add(set7);
        list.add(set3);
        List<Set<Integer>> res = new ArrayList<>();
        res.add(set2);
        res.add(set3);
        res.add(set6);
        res.add(set7);
        res.add(set6);
        res.add(set7);
        res.add(set2);
        res.add(set6);
        res.add(set7);
        res.add(set3);
        assertEquals(CollectionsDemo.unionOfSets(list, set), res);
    }


}
